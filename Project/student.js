function lit() {
	var counter = 0;
	var s = document.getElementById("abstract2").value;
	for (var i = 0; i < s.length; i++) {
		if(s[i] == " "){
			counter++;
		}
	}
	if((counter <= 2) && (counter > 0)){
		document.getElementById("review1").style.display = "block";
		document.getElementById("review2").style.display = "block";
	}
	else if((counter <= 2) && (counter == 0)){
		document.getElementById("review1").style.display = "none";
		document.getElementById("review2").style.display = "none";
		document.getElementById("methodology1").style.display = "none";
		document.getElementById("methodology2").style.display = "none";
		document.getElementById("analysis1").style.display = "none";
		document.getElementById("analysis2").style.display = "none";
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Abstract is empty");
	}
	else{
		document.getElementById("review1").style.display = "none";
		document.getElementById("review2").style.display = "none";
		document.getElementById("methodology1").style.display = "none";
		document.getElementById("methodology2").style.display = "none";
		document.getElementById("analysis1").style.display = "none";
		document.getElementById("analysis2").style.display = "none";
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Abstract is more than 800 words");
	}
}

function met() {
	var counter = 0;
	var s = document.getElementById("review2").value;
	for (var i = 0; i < s.length; i++) {
		if(s[i] == " "){
			counter++;
		}
	}
	if((counter <= 800) && (counter > 0)){
		document.getElementById("methodology1").style.display = "block";
		document.getElementById("methodology2").style.display = "block";
	}
	else if((counter <= 800) && (counter == 0)){
		document.getElementById("methodology1").style.display = "none";
		document.getElementById("methodology2").style.display = "none";
		document.getElementById("analysis1").style.display = "none";
		document.getElementById("analysis2").style.display = "none";
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Literature review is empty");
	}
	else{
		document.getElementById("methodology1").style.display = "none";
		document.getElementById("methodology2").style.display = "none";
		document.getElementById("analysis1").style.display = "none";
		document.getElementById("analysis2").style.display = "none";
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Literature review is more than 800 words");
	}
}

function ana() {
	var counter = 0;
	var s = document.getElementById("methodology2").value;
	for (var i = 0; i < s.length; i++) {
		if(s[i] == " "){
			counter++;
		}
	}

	if((counter <= 800) && (counter > 0)){
		document.getElementById("analysis1").style.display = "block";
		document.getElementById("analysis2").style.display = "block";
	}
	else if((counter <= 800) && (counter == 0)){
		document.getElementById("analysis1").style.display = "none";
		document.getElementById("analysis2").style.display = "none";
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Methodology is empty");
	}
	else{
		document.getElementById("analysis1").style.display = "none";
		document.getElementById("analysis2").style.display = "none";
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Methodology is more than 800 words");
	}
}

function con() {
	var counter = 0;
	var s = document.getElementById("analysis2").value;
	for (var i = 0; i < s.length; i++) {
		if(s[i] == " "){
			counter++;
		}
	}

	if((counter <= 800) && (counter > 0)){
		document.getElementById("conclusion1").style.display = "block";
		document.getElementById("conclusion2").style.display = "block";
	}
	else if((counter <= 800) && (counter == 0)){
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Analysis is empty");
	}
	else{
		document.getElementById("conclusion1").style.display = "none";
		document.getElementById("conclusion2").style.display = "none";
		document.getElementById("button").style.display = "none";
		alert("Analysis is more than 800 words");
	}
}

function final() {
	var counter = 0;
	var s = document.getElementById("conclusion2").value;
	for (var i = 0; i < s.length; i++) {
		if(s[i] == " "){
			counter++;
		}
	}

	if((counter <= 400) && (counter > 0)){
		document.getElementById("button").style.display = "block";
	}
	else if((counter <= 400) && (counter == 0)){
		document.getElementById("button").style.display = "none";
		alert("Conclusion is empty");
	}
	else{
		document.getElementById("button").style.display = "none";
		alert("Conclusion is more than 400 words");
	}
}

// document.getElementById("abstract2").addEventListener("keyup", countWords);
// function countWords(){
// 	var s = document.getElementById("abstract2").value;
// 	s = s.split(' ').length;
// 	document.write(s);
// }
