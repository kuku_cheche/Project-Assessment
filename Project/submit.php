<!DOCTYPE html>
<html>
<head>
	<title>Project Assessment Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- <script type="text/javascript" src="student.js"></script> -->
	<style type="text/css">
		body, html{
			height: 100%;
			font-family: "Century Gothic";
			overflow: auto;
		}

		#bg {
			background-image: url(img/animation-flat-line-knowledge-and-creative-education-graphic-design-flat-creativity-school-and-stationary-sign-and-symbol-education-icon-with-isolated-background-concept-in-4k_brta71_kl_thumbnail-full14.png);
			height: 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			opacity: 0.2;
 		}

 		#mydiv{
			position: fixed;
			top: 30%;
			left: 35%;
			width: 60em;
			height: 35em;
			margin-top: -9em;
			margin-left: -15em;
			border-top: solid;
			border-right: solid;
			border-top-color: indigo;
			border-right-color: indigo;
			background-color: ghostwhite;
			border-radius: 2em;
		}

		img{
			margin-top: 1em;
			margin-left: 4em;
			border-radius: 5px;
			border: 1px solid black;
			height: 10em;
			width: 10em;
		}

		#link{
			top: 50%;
			left: 50%;
			padding-top: 5em;
			margin-top: 2em;
			text-align: center;
			height: 62.7%;
			border-top: 1px solid black;
			border-bottom-left-radius: 98px;
			background-color: plum;
			font-weight: bold;
		}

		.edit{
			border: 1px solid black;
			border-top-right-radius: 98px;
			height: 5%;
			width: 100%;
			text-align: center;
			background-color: purple;
			color: white;
		}

		.form{
			border: 1px solid black;
			border-bottom-right-radius: 0.2em;
			height: 95%;
			width: 100%;
			background-color: ghostwhite;
			overflow: auto;
		}

		form{
			padding-top: 1em;
			padding-bottom: 1em;
			padding-left: 1em; 
			padding-right: 1em;
		}

		textarea{
			border: 1px solid purple;
			background-color: ghostwhite;
			width: 35em;
			max-width: auto;
			padding-right: 1em;
		}

		header{
			width: 17em;
			border-top-right-radius: 98px;
			background-color: purple;
			color: white;
			font-weight: bold;
			padding-left: 0.5em;
		}

		form > div{
			position: relative;
			
		}

		
		a{
			text-decoration: none;
			color: white;
		}

		a:hover{
			text-decoration: none;
			color: darkred;
			border-bottom: 2px solid purple;
		}

	</style>
</head>
<body>
	<div class="container-fluid" id="bg">
	</div>
	<div id="mydiv">
		<div style="float: left; height: 100%; width: 30%;" id="innerdiv">
			<div id="image"><img src="#" alt="Student Picture"></div>
			<div id="link">
				<a href="submit.php">Submit Project</a><br><br>
				<a href="form.php">Update Bio</a><br><br>
				<a href="result.php">Check Result</a><br><br>
			    <p id="para">Successfully updated bio!</p>
			</div>
		</div>
		<div style="float: right; height: 100%; width: 70%;">
			<div class="edit"><h6><b>Submit Project</b></h6></div>
			<div class="form">
				<form class="col-md-10" action="submit.php" method="post" style="position: relative; margin-top: 0.1em; margin-left: 1em;" >
					<div>
						<header id="abstract1">Abstract <small><b>(800 words maximum)</b></small></header>
						<textarea id="abstract2" name="abstract" rows="10" cols="150" required="required" onblur="lit();"></textarea>
					</div><br>
					<div>
						<header id="review1" style="display: none; width: 20em;">Literature Review <small><b>(800 words maximum)</b></small></header>
						<textarea id="review2" style="display: none;" name="review" rows="10" cols="150" required="required" onblur="met();"></textarea>
					</div><br>
					<div>
						<header id="methodology1" style="display: none;">Methodology <small><b>(800 words maximum)</b></small></header>
						<textarea 0 id="methodology2" style="display: none;" name="methodology" rows="10" cols="150" required="required" onblur="ana();"></textarea>
					</div><br>
					<div>
						<header id="analysis1" style="display: none;">Analysis <small><b>(800 words maximum)</b></small></header>
						<textarea id="analysis2" style="display: none;" name="analysis" rows="10" cols="150" required="required" onblur="con();"></textarea>
					</div><br>
					<div>
						<header id="conclusion1" style="display: none;">Conclusion <small><b>(400 words maximum)</b></small></header>
						<textarea id="conclusion2" style="display: none;" name="conclusion" rows="10" cols="150" required="required" onblur="final();"></textarea>
					</div>
					<div>
						<input type= "submit" name ="submit" value="submit" id="button" style="width: 100px; height: 50px; background-color: purple; color: white; border: 3px solid #EEE; border-radius: 10px; margin-left: 15em; margin-top: 2em; font-weight: bold; display: none;">
					</div>
					<br>
				</form>	
			</div>
		</div>
	</div>

	<script type="text/javascript">
		document.getElementById('para').style.display = "none";
		function lit() {
			var counter = 0;
			var s = document.getElementById("abstract2").value;
			for (var i = 0; i < s.length; i++) {
				if(s[i] == " "){
					counter++;
				}
			}
			if((counter <= 800) && (counter > 0)){
				document.getElementById("review1").style.display = "block";
				document.getElementById("review2").style.display = "block";
			}
			else if((counter <= 800) && (counter == 0)){
				document.getElementById("review1").style.display = "none";
				document.getElementById("review2").style.display = "none";
				document.getElementById("methodology1").style.display = "none";
				document.getElementById("methodology2").style.display = "none";
				document.getElementById("analysis1").style.display = "none";
				document.getElementById("analysis2").style.display = "none";
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Abstract is empty");
			}
			else{
				document.getElementById("review1").style.display = "none";
				document.getElementById("review2").style.display = "none";
				document.getElementById("methodology1").style.display = "none";
				document.getElementById("methodology2").style.display = "none";
				document.getElementById("analysis1").style.display = "none";
				document.getElementById("analysis2").style.display = "none";
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Abstract is more than 800 words");
			}
		}

		function met() {
			var counter = 0;
			var s = document.getElementById("review2").value;
			for (var i = 0; i < s.length; i++) {
				if(s[i] == " "){
					counter++;
				}
			}
			if((counter <= 800) && (counter > 0)){
				document.getElementById("methodology1").style.display = "block";
				document.getElementById("methodology2").style.display = "block";
			}
			else if((counter <= 800) && (counter == 0)){
				document.getElementById("methodology1").style.display = "none";
				document.getElementById("methodology2").style.display = "none";
				document.getElementById("analysis1").style.display = "none";
				document.getElementById("analysis2").style.display = "none";
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Literature review is empty");
			}
			else{
				document.getElementById("methodology1").style.display = "none";
				document.getElementById("methodology2").style.display = "none";
				document.getElementById("analysis1").style.display = "none";
				document.getElementById("analysis2").style.display = "none";
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Literature review is more than 800 words");
			}
		}

		function ana() {
			var counter = 0;
			var s = document.getElementById("methodology2").value;
			for (var i = 0; i < s.length; i++) {
				if(s[i] == " "){
					counter++;
				}
			}

			if((counter <= 800) && (counter > 0)){
				document.getElementById("analysis1").style.display = "block";
				document.getElementById("analysis2").style.display = "block";
			}
			else if((counter <= 800) && (counter == 0)){
				document.getElementById("analysis1").style.display = "none";
				document.getElementById("analysis2").style.display = "none";
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Methodology is empty");
			}
			else{
				document.getElementById("analysis1").style.display = "none";
				document.getElementById("analysis2").style.display = "none";
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Methodology is more than 800 words");
			}
		}

		function con() {
			var counter = 0;
			var s = document.getElementById("analysis2").value;
			for (var i = 0; i < s.length; i++) {
				if(s[i] == " "){
					counter++;
				}
			}

			if((counter <= 800) && (counter > 0)){
				document.getElementById("conclusion1").style.display = "block";
				document.getElementById("conclusion2").style.display = "block";
			}
			else if((counter <= 800) && (counter == 0)){
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Analysis is empty");
			}
			else{
				document.getElementById("conclusion1").style.display = "none";
				document.getElementById("conclusion2").style.display = "none";
				document.getElementById("button").style.display = "none";
				alert("Analysis is more than 800 words");
			}
		}

		function final() {
			var counter = 0;
			var s = document.getElementById("conclusion2").value;
			for (var i = 0; i < s.length; i++) {
				if(s[i] == " "){
					counter++;
				}
			}

			if((counter <= 400) && (counter > 0)){
				document.getElementById("button").style.display = "block";
			}
			else if((counter <= 400) && (counter == 0)){
				document.getElementById("button").style.display = "none";
				alert("Conclusion is empty");
			}
			else{
				document.getElementById("button").style.display = "none";
				alert("Conclusion is more than 400 words");
			}
		}
	</script>
<?php

	include 'login.php';
if(isset($_POST["submit"])){
	if(isset($_POST["abstract"]))
		$abstract = $_POST["abstract"];
	if(isset($_POST["review"]))
		$review = $_POST["review"];
	if(isset($_POST["methodology"]))
		$methodology = $_POST["methodology"];
	if(isset($_POST["analysis"]))
		$analysis = $_POST["analysis"];
	if(isset($_POST["conclusion"]))
		$conclusion = $_POST["conclusion"];
	if(isset($_SESSION["matricnumber"]))
		$matric = $_SESSION["matricnumber"];
	else
		$matric = 140805010;

	$id = crc32($matric);
	$conn = new mysqli($hn, $un, $pw, $db);
	if($conn -> connect_error)
		die($conn -> connect_error);
	$query = "INSERT into project(ProjectID, s_matricnumber, p_abstract, p_literaturereview, p_methodology, p_analysis, p_conclusion) VALUES($id, $matric,'". $abstract."','". $review."', '".$methodology."','". $analysis."','". $conclusion."')";
	$result = $conn -> query($query);
	if(!$result)
		die ($conn -> error);

	$conn->close();

	// $hostname = 'localhost';
	// $username = 'root';
	// $password = '';
	

	// $_SESSION["abstract"] = $abstract;
	// $_SESSION["review"] = $review;
	// $_SESSION["methodology"] = $methodology;
	// $_SESSION["analysis"] = $analysis;
	// $_SESSION["conclusion"] = $conclusion;

	// echo $abstract."here i am";
	// echo $_SESSION["review"];
	// echo $_SESSION["methodology"];


	// try{
	// 	$conn = new PDO("mysql:host=$hostname;dbname=student", $username, $password);
	// 	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// }
	// catch(PDOException $e){
	// 	echo "<script>alert('Connection failed');</script>: ".$e->getMessage();
	// }

	// $id =  140805008;
	// $fh = fopen("$fname.txt", 'wb') or die("Failed to create file");
	// fwrite($fh, "$abhead$abstract$rwhead$review$mthead$methodology$anhead$analysis$cnhead$conclusion") or die("Could not write to file");
	// fclose($fh);

	// $filepath = "$fname.txt";
	// $blob = fopen($filepath, 'rb');
	// $sql = "INSERT into project(ProjectID, s_matricnumber, p_abstract, p_literaturereview, p_methodology, p_analysis, p_conclusion) VALUES (:id, :matric, :abstract, :review, :methodology, :analysis, :conclusion)";
	// $stmt = $conn->prepare($sql);
	// $stmt-> bindParam(':id', $id);
	// $stmt-> bindParam(':matric', $matric);
	// $stmt-> bindParam(':abstract', $abstract);
	// $stmt-> bindParam(':review', $review);
	// $stmt-> bindParam(':methodology', $methodology);
	// $stmt-> bindParam(':analysis', $analysis);
	// $stmt-> bindParam(':conclusion', $conclusion);
	// echo'here ahain';
	// $id =140805008;
	// $matric = 140805008;
	// $abstract = $_SESSION["abstract"];
	// $review = $_SESSION["review"];
	// $methodology = $_SESSION["methodology"];
	// $analysis = $_SESSION["analysis"];
	// $conclusion = $_SESSION["conclusion"];


	// return $stmt->execute();

	// function sanitize($var){
	// 	$var = stripslashes($var);
	// 	$Var = strip_tags($var);
	// 	$var = htmlentities($var);
	// 	return $var;
	// }
	// $conn = null;
	
}
?>
</body>
</html>