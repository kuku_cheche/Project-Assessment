<?php
	include 'login.php';
	$matric = "140805008";
	$conn = new mysqli($hn, $un, $pw, $db);
	if($conn -> connect_error)
		die($conn -> connect_error);
	$query = "SELECT * FROM student WHERE s_matricnumber = '$matric'";
	$result = $conn -> query($query);
	if(!$result)
		die ($conn -> error);
	$y = 0;
	$s_matricnumber = $matric;
	$result -> data_seek($y);
	$s_firstname = $result -> fetch_assoc()['s_firstname'];
	$result -> data_seek($y);
	$s_lastname = $result -> fetch_assoc()['s_lastname'];
	$result -> data_seek($y);
	$s_mail = $result -> fetch_assoc()['s_mail'];
	$result -> data_seek($y);
	$s_department = $result -> fetch_assoc()['s_department'];
	$result -> data_seek($y);
	$s_phonenumber = $result -> fetch_assoc()['s_phonenumber'];
	$result -> data_seek($y);
	$s_address = $result -> fetch_assoc()['s_address'];
	$result -> data_seek($y);
	$s_password = $result -> fetch_assoc()['s_password'];
	$result -> data_seek($y);
	$s_maritalstatus = $result -> fetch_assoc()['s_maritalstatus'];
	$result -> data_seek($y);
	$s_religion = $result -> fetch_assoc()['s_religion'];
	$result -> data_seek($y);
	$s_dob = $result -> fetch_assoc()['s_dob'];
	$result -> data_seek($y);
	$s_nationality = $result -> fetch_assoc()['s_nationality'];
	$result -> data_seek($y);
	$s_origin = $result -> fetch_assoc()['s_origin'];
	$result -> data_seek($y);
	$s_localgovernment = $result -> fetch_assoc()['s_localgovernment'];
	$result -> data_seek($y);
	$s_title = $result -> fetch_assoc()['s_title'];
	$result -> close();



	$title=$sname=$fname=$mname=$dept=$mstatus=$religion=$dob=$nationality=$state=$lga=$address=$tnum=$email = "";
	$titleErr=$snameErr=$fnameErr=$mnameErr=$deptErr=$mstatusErr=$religionErr=$dobErr=$nationalityErr=$stateErr=$lgaErr=$addressErr=$tnumErr=$emailErr= "";
	if(($_SERVER["REQUEST_METHOD"]) == "POST"){
		if(empty($_POST["title"])){
			$titleErr = "*Title is required";
		}
		else{
			$title = sanitize($_POST["title"]);
		}

		if(empty($_POST["surname"])){
			$snameErr = "*Surname is required";
		}
		else{
			$sname = sanitize($_POST["surname"]);
		}

		if(empty($_POST["firstname"])){
			$fnameErr = "*Firstname is required";
		}
		else{
			$fname = sanitize($_POST["firstname"]);
		}

		if(empty($_POST["middlename"])){
			$mnameErr = "*Middlename is required";
		}
		else{
			$mname = sanitize($_POST["middlename"]);
		}

		if(empty($_POST["dept"])){
			$deptErr = "*Department is required";
		}
		else{
			$dept = sanitize($_POST["dept"]);
		}

		if(empty($_POST["mstatus"])){
			$mstatusErr = "*Marriage status is required";
		}
		else{
			$mstatus = sanitize($_POST["mstatus"]);
		}

		if(empty($_POST["religion"])){
			$religionErr = "*Religion is required";
		}
		else{
			$religion = sanitize($_POST["religion"]);
		}

		if(empty($_POST["DOB"])){
			$dobErr = "*Date of birth is required";
		}
		else{
			$dob = sanitize($_POST["DOB"]);
		}

		if(empty($_POST["nationality"])){
			$nationalityErr = "*Nationality is required";
		}
		else{
			$nationality = sanitize($_POST["nationality"]);
		}

		if(empty($_POST["state"])){
			$stateErr = "*State of origin is required";
		}
		else{
			$state = sanitize($_POST["state"]);
		}

		if(empty($_POST["LGA"])){
			$lgaErr = "*Local government area is required";
		}
		else{
			$lga = sanitize($_POST["LGA"]);
		}

		if(empty($_POST["address"])){
			$addressErr = "*Home address is required";
		}
		else{
			$address = sanitize($_POST["address"]);
		}

		if(empty($_POST["phone_no"])){
			$tnumErr = "*Phone number is required";
		}
		else{
			$tnum = sanitize($_POST["phone_no"]);
		}
		
		if(empty($_POST["email"])){
			$emailErr = "*Email address is required";
		}
		else{
			$email = sanitize($_POST["email"]);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      			$emailErr = "Invalid email format"; 
   			}
		}

		$query = "UPDATE student SET s_firstname = '$fname', s_lastname = '$sname', s_title = '$title', s_mail = '$email', s_localgovernment = '$lga', s_origin = '$state', s_nationality = '$nationality', s_phonenumber = '$tnum', s_address = '$address', s_department = '$dept', s_dob = '$dob', s_maritalstatus = '$mstatus', s_religion = '$religion' WHERE s_matricnumber ='$matric'";

		$result = $conn -> query($query);
		if(!$result)
			die ($conn -> error);
		else
			echo "<script>alert('Details sucessfully updated')</script>";
		// $result->close();
	}

	function sanitize($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	$conn -> close();
	//echo "<script>alert('Details sucessfully updated')</script>";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Project Assessment Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="update.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">
		.error{
			color: red;
		}
		span{
			float: right;
		}
	</style>
</head>
<body>
	<header>
		<img src="img/small-ogo-uti.png">
		<h3 class="greeting">Welcome, Chidera</h3>
	</header>
	<nav class="menu">
		<div  class="pic"><img src="#" alt="Student picture" style="margin: 2vw 0vw 0vw 0vw; width:13vw; height:15vw; padding: 2px 2px 2px 2px; margin-left: px;"></div><br><br><br>
		<div class="menu2"><a href="dummypage3.php" target="_self"><span style="margin-right: 5px; position: absolute; left: 38px;"><img src="img/enter-arrow.png" class="submit"></span>Submit Project</a><br><br>
		<a href="update.php" target="_self"><span style="margin-right: 5px;"><img src="img/refresh-left-arrow.png"></span>Update Biodata</a><br><br>
		<a href="result.php"><span style="margin-right: 5px; position: absolute; left: 40px;"><img src="img/exam.png"></span>Check Result</a><br></div>
	</nav>
	<section class="main_section">
		<div><h2 class="edit">Edit Biodata</h2></div>
		<form method="post" action="<?php echo(htmlspecialchars($_SERVER['PHP_SELF']));?>">
			<div class="form-ui-panel">
				<div class="pane">
					<label>Title:       </label>
					<select class="clear" name="title">
						<option value="Mr" <?php echo $s_title == 'Mr' ? 'selected' : '' ?>>Mr.</option>
						<option value="Miss" <?php echo $s_title == 'Miss' ? 'selected' : '' ?>>Miss.</option>
						<option value="Mrs" <?php echo $s_title == 'Mrs' ? 'selected' : '' ?>>Mrs</option>
						<option value="Dr" <?php echo $s_title == 'Dr' ? 'selected' : '' ?>>Dr.</option>
						<option value="Prof" <?php echo $s_title == 'Prof' ? 'selected' : '' ?>>Prof.</option>
					</select>
					<span class="error"><?php echo $titleErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Surname: </label><input type="text" name="surname" value="<?php echo $s_lastname;?>">
					<span class="error"><?php echo $snameErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Firstname: </label><input type="text" name="firstname" value="<?php echo $s_firstname;?>">
					<span class="error"><?php echo $fnameErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Middlename: </label><input type="text" name="middlename" value="<?php echo $mname;?>">
					<span class="error"><?php echo $mnameErr; ?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Department: </label><input type="text" name="dept"  value="<?php echo $s_department;?>">
					<span class="error"><?php echo $deptErr; ?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Marital Status: </label>
					<select class="clear" name="mstatus">
						<option value="Not Set">Not Set</option>
						<option selected="selected" value="Single">Single</option>
						<option value="Married">Married</option>
						<option value="Divorced">Divorced</option>
						<option value="Widowed">Widowed</option>
					</select>
					<span class="error"><?php echo $mstatusErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Religion: </label>
					<select class="clear" name="religion">
						<option value="Not Set">Not Set</option>
						<option selected="selected" value="Christianity">Christianity</option>
						<option value="Islam">Islam</option>
						<option value="Other">Other</option>
					</select>
					<span class="error"><?php echo $religionErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Date of Birth: </label><input type="date" name="DOB"  value="<?php echo $s_dob;?>">
					<span class="error"><?php echo $dobErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Nationality: </label><input type="text" name="nationality"  value="<?php echo $s_nationality;?>">
					<span class="error"><?php echo $nationalityErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>State of Origin: </label><input type="text" name="state"  value="<?php echo $s_origin;?>">
					<span class="error"><?php echo $stateErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Local Government: </label><input type="text" name="LGA"  value="<?php echo $s_localgovernment;?>">
					<span class="error"><?php echo $lgaErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label class="address">Home Address: </label><textarea name="address"><?php echo $s_address;?></textarea>
					<span class="error"><?php echo $addressErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Telephone Number: </label><input type="text" name="phone_no"  value="<?php echo $s_phonenumber;?>">
					<span class="error"><?php echo $tnumErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>E-mail Address: </label><input type="text" name="email"  value="<?php echo $s_mail;?>">
					<span class="error"><?php echo $emailErr;?></span>
					<br class="clear">
				</div>
				<div>
					<button id="button" style="width: 100px; height: 50px; background-color: purple; color: white; border: 3px solid #EEE; border-radius: 10px; margin-left: 450px; font-family: 'Century Gothic'; font-weight: bold; ">Submit</button>
				</div><br>
				<p id="para">Hello dear</p>
			</div>
		</form>
	</section>
	<footer><center style="color: white;">&copy;Copyright CSC405 Group 9</center></footer>
	<script type="text/javascript">
		document.getElementById('para').style.display = "none";
	</script>
</body>
</html>
