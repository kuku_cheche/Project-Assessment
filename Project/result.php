<?php
	session_start();
	include("../admin1/database.php");

$query = "SELECT DISTINCT AVG(RESULT.r_abstract) as abstract,AVG(RESULT.r_literaturereview) as review,AVG(RESULT.r_methodology) as methodology,AVG(RESULT.r_analysis) as analysis,AVG(RESULT.r_conclusion) as conclusion,AVG(RESULT.r_total) AS total FROM RESULT GROUP BY RESULT.ProjectID HAVING RESULT.ProjectID=".crc32($_SESSION['matricnumber']);
	$result=$conn->query($query);
	$result->data_seek(0);
	$abstractscore = $result->fetch_assoc()['abstract'];
	$result->data_seek(0);
	$reviewscore = $result->fetch_assoc()['review'];
	$result->data_seek(0);
	$methodologyscore = $result->fetch_assoc()['methodology'];
	$result->data_seek(0);
	$analysisscore = $result->fetch_assoc()['analysis'];
	$result->data_seek(0);
	$conclusionscore = $result->fetch_assoc()['conclusion'];
	$result->data_seek(0);
	$totalscore = $result->fetch_assoc()['total'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Project Assessment Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="student.js"></script>
	<style type="text/css">
		body, html{
			height: 100%;
			font-family: "Century Gothic";
			overflow: auto;
		}

		#bg {
			background-image: url(img/animation-flat-line-knowledge-and-creative-education-graphic-design-flat-creativity-school-and-stationary-sign-and-symbol-education-icon-with-isolated-background-concept-in-4k_brta71_kl_thumbnail-full14.png);
			height: 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			opacity: 0.2;
 		}

 		#mydiv{
			position: fixed;
			top: 30%;
			left: 35%;
			width: 60em;
			height: 35em;
			margin-top: -9em;
			margin-left: -15em;
			border-top: solid;
			border-right: solid;
			border-top-color: indigo;
			border-right-color: indigo;
			background-color: ghostwhite;
			border-radius: 2em;
		}

		img{
			margin-top: 1em;
			margin-left: 4em;
			border-radius: 5px;
			border: 1px solid black;
			height: 10em;
			width: 10em;
		}

		#link{
			top: 50%;
			left: 50%;
			padding-top: 5em;
			margin-top: 2em;
			text-align: center;
			height: 62.7%;
			border-top: 1px solid black;
			border-bottom-left-radius: 98px;
			background-color: plum;
			font-weight: bold;
		}

		.edit{
			border: 1px solid black;
			border-top-right-radius: 98px;
			height: 5%;
			width: 100%;
			text-align: center;
			background-color: purple;
			color: white;
		}

		.form{
			border: 1px solid black;
			border-bottom-right-radius: 0.2em;
			height: 95%;
			width: 100%;
			background-color: ghostwhite;
			overflow: auto;
		}

		input{
			border:none;
			border-bottom: 2px solid purple;
			border-bottom-right-radius: 2px;
			border-bottom-left-radius: 2px;
			background-color: ghostwhite;
			width: 20em;
			padding-right: 1em;
			text-align: center;
		}

		label{
			font-weight: lighter;
			color: purple;
			margin-left: 3em;
			margin-right: 2em;
			margin-top: 2em;
		}

		a{
			text-decoration: none;
			color: white;
		}

		a:hover{
			text-decoration: none;
			color: darkred;
			border-bottom: 2px solid purple;
		}

	</style>
</head>
<body>
	<div class="container-fluid" id="bg">
	</div>
	<div id="mydiv">
		<div style="float: left; height: 100%; width: 30%;" id="innerdiv">
			<!-- <nav class="menu">
				<div  class="pic"><img src="#" alt="Student picture" style="margin: 2vw 0vw 0vw 0vw; width:13vw; height:15vw; padding: 2px 2px 2px 2px; margin-left: px;"></div><br><br><br>
			<div class="menu2"><a href="dummypage3.php" target="_self"><span style="margin-right: 5px; position: absolute; left: 38px;"><img src="img/enter-arrow.png" class="submit"></span>Submit Project</a><br><br>
			<a href="update.php" target="_self"><span style="margin-right: 5px;"><img src="img/refresh-left-arrow.png"></span>Update Biodata</a><br><br>
			<a href="result.php"><span style="margin-right: 5px; position: absolute; left: 40px;"><img src="img/exam.png"></span>Check Result</a><br></div>
			</nav> -->
			<div id="image"><img src="#" alt="Student Picture"></div>
			<div id="link">
				<a href="submit.php">Submit Project</a><br><br>
				<a href="form.php">Update Bio</a><br><br>
				<a href="result.php">Check Result</a><br><br>
			    <p id="para">Successfully updated bio!</p>
			</div>
		</div>
		<div style="float: right; height: 100%; width: 70%;">
			<div class="edit"><h6><b>Result</b></h6></div>
			<div class="form">
				<label>Abstract: </label><input type="text" name="abstract" style="position: relative; left: 4.3em;" value="<?php echo $abstractscore;?>"><br><br>
				<label>Literature Review: </label><input type="text" name="review" value="<?php echo $reviewscore;?>"><br><br>
				<label>Methodology: </label><input type="text" name="methodology" style="position: relative; left: 2em;" value="<?php echo $methodologyscore;?>"><br><br>
				<label>Analysis: </label><input type="text" name="analysis" style="position: relative; left: 5em;" value="<?php echo $analysisscore;?>"><br><br>
				<label>Conclusion: </label><input type="text" name="conclusion" style="position: relative; left: 3.4em;" value="<?php echo $conclusionscore;?>"><br><br>
				<label>Total: </label><input type="text" name="conclusion" style="position: relative; left: 6.2em;" value="<?php echo $totalscore;?>">
			</div>
		</div>
	</div>

	<script type="text/javascript">
		document.getElementById('para').style.display = "none";
		
	</script>
</body>
</html>
