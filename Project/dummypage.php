<!DOCTYPE html>
<html>
<head>
	<title>Project Assessment Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="student.js"></script>
	<link rel="stylesheet" type="text/css" href="update.css">
</head>
<body background-repeat: repeat-x;">
<div class="container-fluid">
	<p class="page-header text-lead text-danger" style="font-size: 3vw;">Welcome,<?php $name="Chidera"; echo"$name";?></p>
	<div class="col-md-2 text-center">
		<img src="" alt="Student picture"><br>
		<a href="#">Submit Project</a><br>
		<a href="#">Update Bio</a><br>
		<a href="#">Check Result</a><br>
	</div>
	<form class="col-md-10" action="student.php" method="post">
		<div>
			<header id="abstract1">Abstract <small><b>(800 words maximum)</b></small></header>
			<textarea id="abstract2" name="abstract" rows="10" cols="150" required="required" onblur="lit();"></textarea>
		</div>
		<div>
			<header id="review1" style="display: none;">Literature Review <small><b>(800 words maximum)</b></small></header>
			<textarea id="review2" style="display: none;" name="review" rows="10" cols="150" required="required" onblur="met();"></textarea>
		</div>
		<div>
			<header id="methodology1" style="display: none;">Methodology <small><b>(800 words maximum)</b></small></header>
			<textarea 0 id="methodology2" style="display: none;" name="methodology" rows="10" cols="150" required="required" onblur="ana();"></textarea>
		</div>
		<div>
			<header id="analysis1" style="display: none;">Analysis <small><b>(800 words maximum)</b></small></header>
			<textarea id="analysis2" style="display: none;" name="analysis" rows="10" cols="150" required="required" onblur="con();"></textarea>
		</div>
		<div>
			<header id="conclusion1" style="display: none;">Conclusion <small><b>(400 words maximum)</b></small></header>
			<textarea id="conclusion2" style="display: none;" name="conclusion" rows="10" cols="150" required="required" onblur="final();"></textarea>
		</div>
		<div>
			<button class="btn btn-success" id="button" style="display: none;">Submit Project</button>
		</div>
		<br>
	</div>
</form>	
</body>
</html>