<?php
session_start();
	include 'login.php';
	$matric = $_SESSION['matricnumber'];
	$conn = new mysqli($hn, $un, $pw, $db);
	if($conn -> connect_error)
		die($conn -> connect_error);
	$query = "SELECT * FROM student WHERE s_matricnumber = '$matric'";
	$result = $conn -> query($query);
	if(!$result)
		die ($conn -> error);
	$y = 0;
	$s_matricnumber = $matric;
	$result -> data_seek($y);
	$s_firstname = $result -> fetch_assoc()['s_firstname'];
	$result -> data_seek($y);
	$s_lastname = $result -> fetch_assoc()['s_lastname'];
	$result -> data_seek($y);
	$s_mail = $result -> fetch_assoc()['s_mail'];
	$result -> data_seek($y);
	$s_department = $result -> fetch_assoc()['s_department'];
	$result -> data_seek($y);
	$s_phonenumber = $result -> fetch_assoc()['s_phonenumber'];
	$result -> data_seek($y);
	$s_address = $result -> fetch_assoc()['s_address'];
	$result -> data_seek($y);
	$s_password = $result -> fetch_assoc()['s_password'];
	$result -> data_seek($y);
	$s_maritalstatus = $result -> fetch_assoc()['s_maritalstatus'];
	$result -> data_seek($y);
	$s_religion = $result -> fetch_assoc()['s_religion'];
	$result -> data_seek($y);
	$s_dob = $result -> fetch_assoc()['s_dob'];
	$result -> data_seek($y);
	$s_nationality = $result -> fetch_assoc()['s_nationality'];
	$result -> data_seek($y);
	$s_origin = $result -> fetch_assoc()['s_origin'];
	$result -> data_seek($y);
	$s_localgovernment = $result -> fetch_assoc()['s_localgovernment'];
	$result -> data_seek($y);
	$s_title = $result -> fetch_assoc()['s_title'];
	$result -> close();



	$title=$sname=$fname=$mname=$dept=$mstatus=$religion=$dob=$nationality=$state=$lga=$address=$tnum=$email = "";
	$titleErr=$snameErr=$fnameErr=$mnameErr=$deptErr=$mstatusErr=$religionErr=$dobErr=$nationalityErr=$stateErr=$lgaErr=$addressErr=$tnumErr=$emailErr= "";
	if(($_SERVER["REQUEST_METHOD"]) == "POST"){
		if(empty($_POST["title"])){
			$titleErr = "*Title is required";
		}
		else{
			$title = sanitize($_POST["title"]);
		}

		if(empty($_POST["surname"])){
			$snameErr = "*Surname is required";
		}
		else{
			$sname = sanitize($_POST["surname"]);
		}

		if(empty($_POST["firstname"])){
			$fnameErr = "*Firstname is required";
		}
		else{
			$fname = sanitize($_POST["firstname"]);
		}

		if(empty($_POST["middlename"])){
			$mnameErr = "*Middlename is required";
		}
		else{
			$mname = sanitize($_POST["middlename"]);
		}

		if(empty($_POST["dept"])){
			$deptErr = "*Department is required";
		}
		else{
			$dept = sanitize($_POST["dept"]);
		}

		if(empty($_POST["mstatus"])){
			$mstatusErr = "*Marriage status is required";
		}
		else{
			$mstatus = sanitize($_POST["mstatus"]);
		}

		if(empty($_POST["religion"])){
			$religionErr = "*Religion is required";
		}
		else{
			$religion = sanitize($_POST["religion"]);
		}

		if(empty($_POST["DOB"])){
			$dobErr = "*Date of birth is required";
		}
		else{
			$dob = sanitize($_POST["DOB"]);
		}

		if(empty($_POST["nationality"])){
			$nationalityErr = "*Nationality is required";
		}
		else{
			$nationality = sanitize($_POST["nationality"]);
		}

		if(empty($_POST["state"])){
			$stateErr = "*State of origin is required";
		}
		else{
			$state = sanitize($_POST["state"]);
		}

		if(empty($_POST["LGA"])){
			$lgaErr = "*Local government area is required";
		}
		else{
			$lga = sanitize($_POST["LGA"]);
		}

		if(empty($_POST["address"])){
			$addressErr = "*Home address is required";
		}
		else{
			$address = sanitize($_POST["address"]);
		}

		if(empty($_POST["phone_no"])){
			$tnumErr = "*Phone number is required";
		}
		else{
			$tnum = sanitize($_POST["phone_no"]);
		}
		
		if(empty($_POST["email"])){
			$emailErr = "*Email address is required";
		}
		else{
			$email = sanitize($_POST["email"]);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      			$emailErr = "Invalid email format"; 
   			}
		}

		$query = "UPDATE student SET s_firstname = '$fname', s_lastname = '$sname', s_title = '$title', s_mail = '$email', s_localgovernment = '$lga', s_origin = '$state', s_nationality = '$nationality', s_phonenumber = '$tnum', s_address = '$address', s_department = '$dept', s_dob = '$dob', s_maritalstatus = '$mstatus', s_religion = '$religion' WHERE s_matricnumber ='$matric'";

		$result = $conn -> query($query);
		if(!$result)
			die ($conn -> error);
		else
			echo "<script>alert('Details sucessfully updated);</script>";
		// $result->close();
	}

	function sanitize($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	$conn -> close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Project Assessment Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="student.js"></script>
	<style type="text/css">
		body, html{
			height: 100%;
			font-family: "Century Gothic";
			overflow: auto;
		}

		#bg {
			background-image: url(img/animation-flat-line-knowledge-and-creative-education-graphic-design-flat-creativity-school-and-stationary-sign-and-symbol-education-icon-with-isolated-background-concept-in-4k_brta71_kl_thumbnail-full14.png);
			height: 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			opacity: 0.2;
 		}

 		#mydiv{
			position: fixed;
			top: 30%;
			left: 35%;
			width: 60em;
			height: 35em;
			margin-top: -9em;
			margin-left: -15em;
			border-top: solid;
			border-right: solid;
			border-top-color: indigo;
			border-right-color: indigo;
			background-color: ghostwhite;
			border-radius: 2em;
		}

		img{
			margin-top: 1em;
			margin-left: 4em;
			border-radius: 5px;
			border: 1px solid black;
			height: 10em;
			width: 10em;
		}

		#link{
			top: 50%;
			left: 50%;
			padding-top: 5em;
			margin-top: 2em;
			text-align: center;
			height: 62.7%;
			border-top: 1px solid black;
			border-bottom-left-radius: 98px;
			background-color: plum;
			font-weight: bold;
		}

		.edit{
			border: 1px solid black;
			border-top-right-radius: 98px;
			height: 5%;
			width: 100%;
			text-align: center;
			background-color: purple;
			color: white;
		}

		.form{
			border: 1px solid black;
			border-bottom-right-radius: 0.2em;
			height: 95%;
			width: 100%;
			background-color: ghostwhite;
			overflow: auto;
		}

		form{
			padding-top: 1em;
			padding-bottom: 1em;
			padding-left: 2em; 
			padding-right: 2em;
		}

		input{
			border:none;
			border-bottom: 2px solid purple;
			border-bottom-right-radius: 2px;
			border-bottom-left-radius: 2px;
			background-color: ghostwhite;
			width: 20em;
			padding-right: 1em;
		}

		select{
			border: none;
			background-color: ghostwhite;
			width: 10em;
			padding-right: 1em;
		}

		textarea{
			border: 1px solid purple;
			background-color: ghostwhite;
			width: 20em;
			padding-right: 1em;
		}

		label{
			font-weight: lighter;
			color: purple;
		}

		.form-ui-panel .pane{
			margin-top: 2em;
			border-bottom: 3px solid #eee;
			border-right: 3px solid #eee;
		}

		a{
			text-decoration: none;
			color: white;
		}

		a:hover{
			text-decoration: none;
			color: darkred;
			border-bottom: 2px solid purple;
		}

	</style>
</head>
<body>
	<div class="container-fluid" id="bg">
	</div>
	<div id="mydiv">
		<div style="float: left; height: 100%; width: 30%;" id="innerdiv">
			<!-- <nav class="menu">
				<div  class="pic"><img src="#" alt="Student picture" style="margin: 2vw 0vw 0vw 0vw; width:13vw; height:15vw; padding: 2px 2px 2px 2px; margin-left: px;"></div><br><br><br>
			<div class="menu2"><a href="dummypage3.php" target="_self"><span style="margin-right: 5px; position: absolute; left: 38px;"><img src="img/enter-arrow.png" class="submit"></span>Submit Project</a><br><br>
			<a href="update.php" target="_self"><span style="margin-right: 5px;"><img src="img/refresh-left-arrow.png"></span>Update Biodata</a><br><br>
			<a href="result.php"><span style="margin-right: 5px; position: absolute; left: 40px;"><img src="img/exam.png"></span>Check Result</a><br></div>
			</nav> -->
			<div id="image"><img src="#" alt="Student Picture"></div>
			<div id="link">
				<a href="submit.php">Submit Project</a><br><br>
				<a href="form.php">Update Bio</a><br><br>
				<a href="result.php">Check Result</a><br><br>
			    <p id="para">Successfully updated bio!</p>
			</div>
		</div>
		<div style="float: right; height: 100%; width: 70%;">
			<div class="edit"><h6><b>Edit Biodata</b></h6></div>
			<div class="form">
				<form method="post" action="<?php echo(htmlspecialchars($_SERVER['PHP_SELF']));?>">
			<div class="form-ui-panel">
				<div class="pane" style="margin-top: 0em;">
					<label>Title:       </label>
					<select class="clear" name="title" style="position: relative; left: 10em;">
						<option value="Mr" <?php echo $s_title == 'Mr' ? 'selected' : '' ?>>Mr.</option>
						<option value="Miss" <?php echo $s_title == 'Miss' ? 'selected' : '' ?>>Miss.</option>
						<option value="Mrs" <?php echo $s_title == 'Mrs' ? 'selected' : '' ?>>Mrs</option>
						<option value="Dr" <?php echo $s_title == 'Dr' ? 'selected' : '' ?>>Dr.</option>
						<option value="Prof" <?php echo $s_title == 'Prof' ? 'selected' : '' ?>>Prof.</option>
					</select>
					<span class="error"><?php echo $titleErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Surname: </label><input type="text" name="surname" value="<?php echo $s_lastname;?>" style="position: relative; left: 8em;">
					<span class="error"><?php echo $snameErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Firstname: </label><input type="text" name="firstname" value="<?php echo $s_firstname;?>" style="position: relative; left: 7.7em;">
					<span class="error"><?php echo $fnameErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>MatricNumber: </label><input type="text" name="middlename" value="<?php echo $mname;?>" style="position: relative; left: 6.2em;">
					<span class="error"><?php echo $mnameErr; ?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Department: </label><input type="text" name="dept"  value="<?php echo $s_department;?>" style="position: relative; left: 6.5em;">
					<span class="error"><?php echo $deptErr; ?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Marital Status: </label>
					<select class="clear" name="mstatus" style="position: relative; left: 5.6em;">
						<option value="Not Set">Not Set</option>
						<option selected="selected" value="Single">Single</option>
						<option value="Married">Married</option>
						<option value="Divorced">Divorced</option>
						<option value="Widowed">Widowed</option>
					</select>
					<span class="error"><?php echo $mstatusErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Religion: </label>
					<select class="clear" name="religion" style="position: relative; left: 8.3em;">
						<option value="Not Set">Not Set</option>
						<option selected="selected" value="Christianity">Christianity</option>
						<option value="Islam">Islam</option>
						<option value="Other">Other</option>
					</select>
					<span class="error"><?php echo $religionErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Date of Birth: </label><input type="date" name="DOB"  value="<?php echo $s_dob;?>" style="position: relative; left: 6.4em;">
					<span class="error"><?php echo $dobErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Nationality: </label><input type="text" name="nationality"  value="<?php echo $s_nationality;?>" style="position: relative; left: 7.3em;">
					<span class="error"><?php echo $nationalityErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>State of Origin: </label><input type="text" name="state"  value="<?php echo $s_origin;?>" style="position: relative; left: 5.7em;">
					<span class="error"><?php echo $stateErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Local Government: </label><input type="text" name="LGA"  value="<?php echo $s_localgovernment;?>" style="position: relative; left: 3.5em;">
					<span class="error"><?php echo $lgaErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label class="address">Home Address: </label><textarea name="address" style="position: relative; left: 5.7em;"><?php echo $s_address;?></textarea>
					<span class="error"><?php echo $addressErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>Telephone Number: </label><input type="text" name="phone_no"  value="<?php echo $s_phonenumber;?>" style="position: relative; left: 3.6em;">
					<span class="error"><?php echo $tnumErr;?></span>
					<br class="clear">
				</div>
				<div class="pane">
					<label>E-mail Address: </label><input type="text" name="email"  value="<?php echo $s_mail;?>" style="position: relative; left: 5.8em;">
					<span class="error"><?php echo $emailErr;?></span>
					<br class="clear">
				</div>
				<div>
					<button id="button" style="width: 100px; height: 50px; background-color: purple; color: white; border: 3px solid #EEE; border-radius: 10px; margin-left: 15em; margin-top: 2em; font-weight: bold; ">Submit</button>
				</div><br>
			</div>
		</form>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		document.getElementById('para').style.display = "none";

	</script>
</body>
</html>
