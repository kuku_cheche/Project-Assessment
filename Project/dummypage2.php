<?php
	$hostname = 'localhost';
	$username = 'root';
	$password = '';
	$abstract = $review = $methodology = $analysis = $conclusion = "";

	if(isset($_POST["abstract"]))
		$abstract = sanitize($_POST["abstract"]);
	if(isset($_POST["review"]))
		$review = sanitize($_POST["review"]);
	if(isset($_POST["methodology"]))
		$methodology = sanitize($_POST["methodology"]);
	if(isset($_POST["analysis"]))
		$analysis = sanitize($_POST["analysis"]);
	if(isset($_POST["conclusion"]))
		$conclusion = sanitize($_POST["conclusion"]);

	try{
		$conn = new PDO("mysql:host=$hostname;dbname=student", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected successfully";

	}
	catch(PDOException $e){
		echo "Connection failed: ".$e->getMessage();
	}

?>