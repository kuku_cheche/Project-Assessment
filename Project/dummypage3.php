<!DOCTYPE html>
<html>
<head>
	<title>Project Assessment Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="update.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="student.js"></script>
</head>
<body>
	<!-- <div class="container-fluid">
		<div class="page-header">
			<img src="img/small-ogo-uti.png" style="position: fixed; top: 1vw;">
			<p style="font-size: 3vw; float: right;">Welcome, Chidera</p>
		</div>
		<div class="col-md-2 text-center" style="background-color: #6C7F92; position: absolute; top: 8vw; float: left;">
			<img src="img/WhatsApp Image 2018-04-09 at 21.17.56.jpeg" alt="Student picture" class="container-left" style="margin: 2vw 0vw 0vw 0vw; width:13vw; height:15vw;"><br><br>
			<a href="student.html" target="_self">Submit Project</a><br>
			<a href="update.html" target="_self">Update Biodata</a><br>
			<a href="#">Check Result</a><br>
		</div>
		<div style="float: right; border: 1px solid blue;">
			<p>me</p>
		</div>
	</div> -->
	<header>
		<img src="img/small-ogo-uti.png">
		<h3 class="greeting">Welcome, Chidera</h3>
	</header>
	<nav class="menu">
		<div  class="pic"><img src="#" alt="Student picture" style="margin: 2vw 0vw 0vw 0vw; width:13vw; height:15vw; padding: 2px 2px 2px 2px; margin-left: px;"></div><br><br><br>
		<div class="menu2"><a href="dummypage3.php" target="_self"><span style="margin-right: 5px; position: absolute; left: 38px;"><img src="img/enter-arrow.png" class="submit"></span>Submit Project</a><br><br>
		<a href="update.php" target="_self"><span style="margin-right: 5px;"><img src="img/refresh-left-arrow.png"></span>Update Biodata</a><br><br>
		<a href="result.php" target="_self"><span style="margin-right: 5px; position: absolute; left: 40px;"><img src="img/exam.png"></span>Check Result</a><br></div>
	</nav>
	<section class="main_section">
		<form class="col-md-10" action="student.php" method="post">
		<div>
			<header id="abstract1">Abstract <small><b>(800 words maximum)</b></small></header>
			<textarea id="abstract2" name="abstract" rows="10" cols="150" required="required" onblur="lit();"></textarea>
		</div>
		<div>
			<header id="review1" style="display: none;">Literature Review <small><b>(800 words maximum)</b></small></header>
			<textarea id="review2" style="display: none;" name="review" rows="10" cols="150" required="required" onblur="met();"></textarea>
		</div>
		<div>
			<header id="methodology1" style="display: none;">Methodology <small><b>(800 words maximum)</b></small></header>
			<textarea 0 id="methodology2" style="display: none;" name="methodology" rows="10" cols="150" required="required" onblur="ana();"></textarea>
		</div>
		<div>
			<header id="analysis1" style="display: none;">Analysis <small><b>(800 words maximum)</b></small></header>
			<textarea id="analysis2" style="display: none;" name="analysis" rows="10" cols="150" required="required" onblur="con();"></textarea>
		</div>
		<div>
			<header id="conclusion1" style="display: none;">Conclusion <small><b>(400 words maximum)</b></small></header>
			<textarea id="conclusion2" style="display: none;" name="conclusion" rows="10" cols="150" required="required" onblur="final();"></textarea>
		</div>
		<div>
			<button class="btn btn-success" id="button" style="display: none;">Submit Project</button>
		</div><br>
	    </form>
	</section>
	<footer><center style="color: white;">&copy;Copyright CSC405 Group 9</cante></footer>
</body>
</html>