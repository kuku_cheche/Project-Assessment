<?php
	session_start();

	$hostname = 'localhost';
	$username = 'root';
	$password = '';
	$abstract = $review = $methodology = $analysis = $conclusion = "";
	$abhead = "ABSTRACT".PHP_EOL;
	$rwhead = "LITERATURE REVIEW".PHP_EOL;
	$mthead = "METHODOLOGY".PHP_EOL;
	$anhead = "ANALYSIS".PHP_EOL;
	$cnhead = "CONCLUSION".PHP_EOL;

	if(isset($_POST["abstract"]))
		$abstract = sanitize($_POST["abstract"]).PHP_EOL.PHP_EOL;
	if(isset($_POST["review"]))
		$review = sanitize($_POST["review"]).PHP_EOL.PHP_EOL;
	if(isset($_POST["methodology"]))
		$methodology = sanitize($_POST["methodology"]).PHP_EOL.PHP_EOL;
	if(isset($_POST["analysis"]))
		$analysis = sanitize($_POST["analysis"]).PHP_EOL.PHP_EOL;
	if(isset($_POST["conclusion"]))
		$conclusion = sanitize($_POST["conclusion"]).PHP_EOL.PHP_EOL;

	// if ($_POST["submit"]) {
	try{
		$conn = new PDO("mysql:host=$hostname;dbname=student", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected successfully";

	}
	catch(PDOException $e){
		echo "Connection failed: ".$e->getMessage();
	}

	$fname = 140805008; $lname = "Adebayo"; $matric = 1765569;
	$fh = fopen("$fname.txt", 'wb') or die("Failed to create file");
	fwrite($fh, "$abhead$abstract$rwhead$review$mthead$methodology$anhead$analysis$cnhead$conclusion") or die("Could not write to file");
	fclose($fh);
	echo "<script type='text/javascript'> alert('Project file succesfully created'); </script>";

	$filepath = "$fname.txt";
	$blob = fopen($filepath, 'rb');
	$sql = "INSERT into PROJ_SUB(Student_FName, Student_LName, Matric_Num, Project) VALUES (:fname, :lname, :matric, :project)";
	$stmt = $conn->prepare($sql);
	$stmt-> bindParam(':fname', $fname);
	$stmt-> bindParam(':lname', $lname);
	$stmt-> bindParam(':matric', $matric);
	$stmt-> bindParam(':project', $blob, PDO::PARAM_LOB);

	return $stmt->execute();

	function sanitize($var){
		$var = stripslashes($var);
		$Var = strip_tags($var);
		$var = htmlentities($var);
		return $var;
	}

	//To counts the number of words
	// $words = explode(" ", "This words has five words");
	// $count = count($words);
	// echo "$count";
	session_destroy();
?>